import React from "react";

const SelectedProductsTree = ({
  selectedProducts,
  selectedProductVariants,
  products,
}) => {
  const renderVariantTree = (productId) => {
    const variantsForProduct = selectedProductVariants.filter(
      (v) => v.productId === productId
    );

    if (variantsForProduct.length === 0) {
      return null;
    }

    return (
      <ul>
        {variantsForProduct.map((variant) => (
          <li key={variant.id}>{variant.title}</li>
        ))}
      </ul>
    );
  };

  return (
    <div style={{ marginTop: "0.3rem" }}>
      {products?.map((product) => {
        const productId = product.id;
        const variantsForProduct = selectedProductVariants.filter(
          (v) => v.productId === productId
        );

        if (
          selectedProducts.some((p) => p.id === productId) ||
          variantsForProduct.length > 0
        ) {
          return (
            <div key={productId}>
              <h3>{product.title}</h3>
              {renderVariantTree(productId)}
            </div>
          );
        }

        return null;
      })}
    </div>
  );
};

export default SelectedProductsTree;
