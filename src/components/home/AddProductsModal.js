import React, { useCallback, Fragment } from "react";
import { Modal, Stack, Checkbox, Thumbnail, TextStyle } from "@shopify/polaris";

import Loader from "@/shared/loaders/Loader";

const AddProductsModal = ({
  openProductModal,
  toggleProductModal,
  products,
  productsLoading,
  handleLoadMoreProducts,
  selectedProducts,
  setSelectedProducts,
  selectedProductVariants,
  setSelectedProductVariants,
}) => {
  const toggleVariantCheck = useCallback((isChecked, id, productId, title) => {
    setSelectedProductVariants((prevSelectedVariants) => {
      if (isChecked) {
        setSelectedProducts((prevSelectedProducts) => {
          if (
            !prevSelectedProducts.some((product) => product.id === productId)
          ) {
            return [...prevSelectedProducts, { id: productId, title }];
          }
          return prevSelectedProducts;
        });
        return [...prevSelectedVariants, { id, productId, title }];
      } else {
        return prevSelectedVariants.filter((variant) => variant.id !== id);
      }
    });
  }, []);

  const toggleProductCheck = useCallback(
    (isChecked, id) => {
      setSelectedProducts((prevSelectedProducts) => {
        if (isChecked) {
          // Find the product with the given id
          const productToCheck = products.find((product) => product.id === id);
          // Extract its variants
          const variantsToCheck = productToCheck?.variants || [];

          // Check all variants
          variantsToCheck.forEach((variant) => {
            toggleVariantCheck(true, variant.id, id, variant.title);
          });

          return [...prevSelectedProducts, { id, title: productToCheck.title }];
        } else {
          // Find the product with the given id
          const productToUncheck = products.find(
            (product) => product.id === id
          );
          // Extract its variants
          const variantsToUncheck = productToUncheck?.variants || [];

          // Uncheck all variants
          variantsToUncheck.forEach((variant) => {
            toggleVariantCheck(false, variant.id, id, variant.title);
          });

          return prevSelectedProducts.filter((product) => product.id !== id);
        }
      });
    },
    [products, toggleVariantCheck]
  );

  const formatSelectedProducts = (
    selectedProducts,
    selectedProductVariants,
    products
  ) => {
    const formattedOutput = [];

    products.forEach((product) => {
      const productId = product.id;
      const variantsForProduct = selectedProductVariants.filter(
        (v) => v.productId === productId
      );

      if (
        selectedProducts.some((p) => p.id === productId) ||
        variantsForProduct.length > 0
      ) {
        formattedOutput.push(product.title + ":");
        variantsForProduct.forEach((variant) => {
          formattedOutput.push(`  ${variant.title}`);
        });
      }
    });
    return formattedOutput.join("\n");
  };

  const addProducts = useCallback(() => {
    toggleProductModal();
    const formattedOutput = formatSelectedProducts(
      selectedProducts,
      selectedProductVariants,
      products
    );
    console.log("Selected products and variants are:\n" + formattedOutput);
  }, [selectedProducts, selectedProductVariants, products]);

  return (
    <Modal
      open={openProductModal}
      onClose={toggleProductModal}
      title="All products"
      limitHeight
      primaryAction={{
        content: "Add",
        onAction: addProducts,
      }}
      secondaryActions={[{ content: "Cancel", onAction: toggleProductModal }]}
      onScrolledToBottom={handleLoadMoreProducts}
    >
      {products?.map((product) => (
        <Fragment key={product.id}>
          <Modal.Section>
            <Stack alignment="center" spacing="tight">
              <Checkbox
                label={product?.title}
                labelHidden
                checked={selectedProducts.some((p) => p.id === product.id)}
                onChange={(isChecked, id) =>
                  toggleProductCheck(isChecked, id, product?.title)
                }
                id={product?.id}
              />
              <Thumbnail size="small" source={product?.image_src} />
              <TextStyle>{product?.title}</TextStyle>
            </Stack>
          </Modal.Section>

          {product?.variants?.length > 1 &&
            product?.variants?.map((variant) => (
              <Modal.Section key={variant?.id}>
                <div style={{ paddingLeft: 35 }}>
                  <Stack distribution="equalSpacing">
                    <Checkbox
                      label={variant?.title}
                      checked={selectedProductVariants.some(
                        (v) => v.id === variant.id
                      )}
                      onChange={(isChecked, id) =>
                        toggleVariantCheck(
                          isChecked,
                          id,
                          product?.id,
                          variant?.title
                        )
                      }
                      id={variant?.id}
                    />
                    <TextStyle>US${variant?.price}</TextStyle>
                  </Stack>
                </div>
              </Modal.Section>
            ))}
        </Fragment>
      ))}

      {productsLoading && (
        <Modal.Section>
          <Loader />
        </Modal.Section>
      )}
    </Modal>
  );
};

export default AddProductsModal;
