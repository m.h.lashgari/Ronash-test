import React, { useCallback, useState } from "react";
import { Button, Page } from "@shopify/polaris";

import { useProducts } from "@/home/useProducts";
import AddProductsModal from "@/home/AddProductsModal";
import SelectedProductsTree from "@/home/SelectedProductsTree";

const Home = () => {
  const [products, productsLoading, handleLoadMoreProducts] = useProducts();
  const [openProductModal, setOpenProductModal] = useState(false);

  // For better optimization we must handle state in AddProductsModal, but the reason i do this is because i want show selected products in the home
  const [selectedProducts, setSelectedProducts] = useState([]);
  const [selectedProductVariants, setSelectedProductVariants] = useState([]);

  const toggleProductModal = useCallback(() => {
    setOpenProductModal((openProductModal) => !openProductModal);
  }, []);

  return (
    <Page title="Home" subtitle="This is home page">
      <Button onClick={toggleProductModal}>Open products modal</Button>

      <AddProductsModal
        openProductModal={openProductModal}
        toggleProductModal={toggleProductModal}
        products={products}
        productsLoading={productsLoading}
        handleLoadMoreProducts={handleLoadMoreProducts}
        selectedProducts={selectedProducts}
        setSelectedProducts={setSelectedProducts}
        selectedProductVariants={selectedProductVariants}
        setSelectedProductVariants={setSelectedProductVariants}
      />

      {!openProductModal ? (
        <SelectedProductsTree
          products={products}
          selectedProducts={selectedProducts}
          selectedProductVariants={selectedProductVariants}
        />
      ) : null}
    </Page>
  );
};

export default Home;
